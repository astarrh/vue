# Generated by Django 3.1.3 on 2020-12-02 17:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Project', '0004_auto_20201202_0936'),
    ]

    operations = [
        migrations.AddField(
            model_name='resource',
            name='name',
            field=models.CharField(default=1, max_length=24),
            preserve_default=False,
        ),
    ]
