from django.contrib import admin

from .models import Project, Resource, Task

admin.site.register(Project)
admin.site.register(Resource)
admin.site.register(Task)
